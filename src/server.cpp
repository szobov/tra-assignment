#include <functional>
#include <numeric>
#include <iostream>

#include <assignment/server.h>
#include <assignment/utils.h>

assignment::Server::Server(assignment::Bot& bot) : bot(bot) {}

void assignment::Server::init()
{
    this->server.Post("/goTo", std::bind(&assignment::Server::postGoTo, this->shared_from_this(),
                                         std::placeholders::_1, std::placeholders::_2));
    this->server.Get("/stop", std::bind(&assignment::Server::getStop, this->shared_from_this(),
                                        std::placeholders::_1, std::placeholders::_2));
    this->server.Get("/pause", std::bind(&assignment::Server::getPause, this->shared_from_this(),
                                         std::placeholders::_1, std::placeholders::_2));
    this->server.Get("/resume", std::bind(&assignment::Server::getResume, this->shared_from_this(),
                                          std::placeholders::_1, std::placeholders::_2));
    this->server.Get("/status", std::bind(&assignment::Server::getStatus, this->shared_from_this(),
                                          std::placeholders::_1, std::placeholders::_2));
}

void assignment::Server::start(const assignment::ServerAddress& address)
{
    this->mainThread = std::thread(&assignment::Server::listen, this->shared_from_this(), address);
}

void assignment::Server::listen(const assignment::ServerAddress& address)
{
    std::cout << "Server is started on " << address.host << ":" << address.port << std::endl;
    if (!this->server.listen(address.host.data(), address.port)) {
        std::cout << "Can't start the server" << std::endl;
        assignment::abort();
    }
}

void assignment::Server::stop()
{
    this->server.stop();
    this->mainThread.join();
    std::cout << "Server is stopped" << std::endl;
}

void assignment::Server::setResponse(httplib::Response& resp,
                                     const assignment::Server::HTTP_STATUS status,
                                     const nlohmann::json& body)
{
    resp.status = static_cast<uint16_t>(status);
    resp.set_content(body.dump(), assignment::Server::jsonContentType);
};

void assignment::Server::postGoTo(const httplib::Request& request, httplib::Response& resp)
{
    nlohmann::json respBody;
    if (request.body.empty()) {
        respBody["error"] = "Body shouldn't be empty";
        assignment::Server::setResponse(resp, assignment::Server::HTTP_STATUS::BAD_REQUEST,
                                        respBody);
        return;
    };

    nlohmann::json reqBody = nlohmann::json::parse(request.body, nullptr, false);
    if (reqBody.is_discarded()) {
        respBody["error"] = "Malformed body";
        assignment::Server::setResponse(resp, assignment::Server::HTTP_STATUS::BAD_REQUEST,
                                        respBody);
        return;
    };

    const std::vector<std::string> paramNames = { "x", "y", "theta", "maxVelocity",
                                                  "acceleration" };

    bool check = std::accumulate(paramNames.begin(), paramNames.end(), false,
                                 [&reqBody](bool acc, const std::string& item) {
                                     return acc && reqBody.count(item);
                                 });

    if (check) {
        respBody["error"] = "Missing parameters";
        assignment::Server::setResponse(resp, assignment::Server::HTTP_STATUS::BAD_REQUEST,
                                        respBody);
        return;
    };

    check = std::accumulate(paramNames.begin(), paramNames.end(), true,
                            [&reqBody](bool acc, const std::string& item) {
                                return acc && reqBody[item].is_number_float();
                            });
    if (!check) {
        respBody["error"] = "All parameters should be float";
        assignment::Server::setResponse(resp, assignment::Server::HTTP_STATUS::BAD_REQUEST,
                                        respBody);
        return;
    }

    if ((reqBody["maxVelocity"] <= 0.0) && (reqBody["acceleration"] <= 0.0)) {
        respBody["error"] = "maxVelocity and acceleration should be positive";
        assignment::Server::setResponse(resp, assignment::Server::HTTP_STATUS::BAD_REQUEST,
                                        respBody);
        return;
    }

    assignment::Target target(reqBody["x"], reqBody["y"], reqBody["theta"], reqBody["maxVelocity"],
                              reqBody["acceleration"]);
    this->bot.setTarget(target);
}

void assignment::Server::getStop(const httplib::Request& /*request*/, httplib::Response& resp)
{
    this->bot.stop();
    nlohmann::json respBody;
    respBody["message"] = "bot has been stopped";
    assignment::Server::setResponse(resp, assignment::Server::HTTP_STATUS::OK, respBody);
};

void assignment::Server::getPause(const httplib::Request& /*request*/, httplib::Response& resp)
{
    this->bot.pause();
    nlohmann::json respBody;
    respBody["message"] = "bot has been paused";
    assignment::Server::setResponse(resp, assignment::Server::HTTP_STATUS::OK, respBody);
};

void assignment::Server::getResume(const httplib::Request& /*request*/, httplib::Response& resp)
{
    this->bot.resume();
    nlohmann::json respBody;
    respBody["message"] = "bot has been resumed";
    assignment::Server::setResponse(resp, assignment::Server::HTTP_STATUS::OK, respBody);
};

void assignment::Server::getStatus(const httplib::Request& /*request*/, httplib::Response& resp)
{
    assignment::BotStatus status = this->bot.status();
    nlohmann::json respBody;
    respBody["message"] = "bot status";
    respBody["status"]["pose"]["x"] = status.position.x;
    respBody["status"]["pose"]["y"] = status.position.y;
    respBody["status"]["theta"] = status.theta;
    respBody["status"]["velocity"]["right"] = status.velocity.right;
    respBody["status"]["velocity"]["left"] = status.velocity.left;
    respBody["status"]["velocity"]["linear"] = status.velocity.linear;
    assignment::Server::setResponse(resp, assignment::Server::HTTP_STATUS::OK, respBody);
};
