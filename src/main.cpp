#include <csignal>
#include <iterator>

#include <assignment/control.h>
#include <assignment/server.h>
#include <assignment/utils.h>

int main(int argc, char* argv[])
{
    assignment::Parameters params;
    std::vector<std::string> argvVectored(argv, argv + argc);
    if (!assignment::processProgrammOptions(argc, argvVectored, params)) {
        return 1;
    };

    assignment::Bot bot(params.botParameters);
    std::shared_ptr<assignment::Server> server = std::make_shared<assignment::Server>(bot);
    server->init();

    std::signal(SIGINT, assignment::sigintHandel);

    server->start(params.serverAddress);
    bot.startBot();

    server->stop();

    return 0;
}
