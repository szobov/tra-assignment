#include <assignment/control.h>
#include <assignment/utils.h>

assignment::Bot::Bot(const assignment::BotParameters& params) :
    currentPosition(0.0, 0.0),
    velocity(0.0, 0.0, 0.0, 0.0),
    prevVelocity(0.0, 0.0, 0.0, 0.0),
    wheelRadius(params.wheelRadius),
    wheelDistance(params.wheelDistance),
    acceptanceDistance(params.acceptanceDistance),
    acceptanceOrientation(params.acceptanceOrientation),
    lastTimestamp(std::chrono::high_resolution_clock::time_point::min()) {};

assignment::BotStatus assignment::Bot::status()
{
    std::lock_guard<std::mutex> lg(this->statusLock);
    assignment::BotStatus status(this->currentPosition, this->theta, this->velocity);
    return status;
};

void assignment::Bot::setTarget(const assignment::Target& target)
{
    std::lock_guard<std::mutex> lg(this->wakeupLock);
    this->targets.push(target);
    this->wakeupCV.notify_one();
};

void assignment::Bot::stop()
{
    std::lock_guard<std::mutex> lg(this->wakeupLock);
    this->currentAction = Actions::STOP;
    this->wakeupCV.notify_one();
}

void assignment::Bot::pause()
{
    std::lock_guard<std::mutex> lg(this->wakeupLock);
    this->currentAction = Actions::PAUSE;
    this->wakeupCV.notify_one();
}
void assignment::Bot::resume()
{
    std::lock_guard<std::mutex> lg(this->wakeupLock);
    this->currentAction = Actions::RESUME;
    this->wakeupCV.notify_one();
}

void assignment::Bot::process()
{
    this->currentTimestamp = std::chrono::high_resolution_clock::now();
    if (this->lastTimestamp == std::chrono::high_resolution_clock::time_point::min()) {
        this->lastTimestamp = this->currentTimestamp;
        return;
    };

    this->dtUS =
        std::chrono::duration<double, std::micro>(this->currentTimestamp - this->lastTimestamp)
            .count()
        / 1e6f;

    this->lastTimestamp = this->currentTimestamp;

    this->processState();
}

void assignment::Bot::processState()
{
    switch (this->state) {
    case State::IDLE: {
        if (this->targets.empty()) {
            this->lastTimestamp = std::chrono::high_resolution_clock::time_point::min();
            std::unique_lock<std::mutex> ntLock(this->wakeupLock);
            while (this->targets.empty() && isBotRunning()
                   && (this->currentAction == Actions::RESUME)) {
                {
                    this->wakeupCV.wait_for(ntLock, assignment::Bot::wakeupTimeout);
                }
            }
        };
        this->state = State::MOVE_TO_POINT;
        return;
    }
    case State::MOVE_TO_POINT: {
        double distanceToTarget =
            assignment::calcDistance(this->currentPosition, this->targets.front().position);

        if (distanceToTarget <= this->acceptanceDistance) {
            this->state = State::ROTATE_TO_TARGET_THETA;
            return;
        }

        const double rotationToTarget = assignment::calcRotationToTarget(
            this->currentPosition, this->targets.front().position, this->theta);

        if (rotationToTarget != 0.0) {
            this->state = State::ROTATE_TO_POINT;
            return;
        };

        double desiredLinearVelocity = distanceToTarget / dtUS;
        double expectedLinearVelocity =
            (this->targets.front().acceleration * this->dtUS) + this->prevVelocity.linear;
        double velocityRatio = desiredLinearVelocity / expectedLinearVelocity;
        if (velocityRatio > 0.0) {
            desiredLinearVelocity /= velocityRatio;
        }
        if (desiredLinearVelocity > this->targets.front().maxVelocity) {
            desiredLinearVelocity = this->targets.front().maxVelocity;
        }

        double angularVelosity = desiredLinearVelocity / this->wheelRadius;

        this->setVelocityAndControl(angularVelosity, angularVelosity, 0.0, desiredLinearVelocity);

        return;
    }
    case State::ROTATE_TO_POINT: {
        const double rotationToTarget = assignment::calcRotationToTarget(
            this->currentPosition, this->targets.front().position, this->theta);
        if (rotationToTarget == 0.0) {
            this->state = State::MOVE_TO_POINT;
            return;
        }
        this->rotate(rotationToTarget);
        return;
    }
    case State::ROTATE_TO_TARGET_THETA: {
        double targetAngularDistance =
            shortestAngularDistance(this->theta, this->targets.front().theta);

        if (std::fabs(targetAngularDistance) <= acceptanceDistance) {
            this->state = State::FINISHED;
            return;
        }
        this->rotate(targetAngularDistance);
        return;
    }
    case State::FINISHED: {
        this->idle(/* popTarget = */ true);
        return;
    }
    }
}

void assignment::Bot::idle(bool popTarget)
{
    this->prevVelocity = this->velocity = WheelsVelocity(0.0, 0.0, 0.0, 0.0);
    if (popTarget) {
        std::lock_guard<std::mutex> lg(this->wakeupLock);
        if (!this->targets.empty()) {
            this->targets.pop();
        }
    }
    this->state = State::IDLE;
}

bool assignment::Bot::processAction()
{
    switch (this->currentAction) {
    case Actions::RESUME: {
        return true;
    }
    case Actions::STOP: {
        this->idle(/* popTarget = */ true);
        return false;
    }
    case Actions::PAUSE: {
        this->idle(/* popTarget = */ false);
        return false;
    }
    }
    std::cout << "Unexpected action failure" << std::endl;
    assignment::abort();
    return false;
}

void assignment::Bot::checkActions()
{
    if (this->processAction()) {
        std::unique_lock<std::mutex> ntLock(this->wakeupLock);
        while (this->currentAction != Actions::RESUME && isBotRunning()) {
            this->wakeupCV.wait_for(ntLock, assignment::Bot::wakeupTimeout);
        }
    }
}

void assignment::Bot::rotate(const double targetAngle)
{
    double desiredWheelRotation =
        (this->wheelDistance * targetAngle) / (assignment::Bot::wheelsCount * this->wheelRadius);

    double desiredAngularVelocity = desiredWheelRotation / this->dtUS;
    double desiredTangVelocity = desiredAngularVelocity * this->wheelRadius;
    double expectedTangVelocity =
        (this->targets.front().acceleration * this->dtUS) + this->prevVelocity.tang;
    double velocityRatio = desiredTangVelocity / expectedTangVelocity;
    if (velocityRatio > 0.0) {
        desiredTangVelocity /= velocityRatio;
        desiredAngularVelocity /= velocityRatio;
    }

    this->setVelocityAndControl(desiredAngularVelocity, -desiredAngularVelocity,
                                desiredTangVelocity, 0.0);
}

void assignment::Bot::setVelocityAndControl(const double left, const double right,
                                            const double tang, const double linear)
{
    {
        std::lock_guard<std::mutex> lg(this->statusLock);
        this->velocity = WheelsVelocity(left, right, tang, linear);
        this->control();
    }

    this->prevVelocity = this->velocity;
}

void assignment::Bot::control()
{
    this->theta += (this->wheelRadius / this->wheelDistance)
                   * (this->velocity.right - this->velocity.left) * this->dtUS;

    if (this->velocity.linear != 0.0) {
        double path = this->velocity.linear * this->dtUS;
        this->currentPosition.x += std::cos(this->theta) * path;
        this->currentPosition.y += std::sin(this->theta) * path;
    }
}

void assignment::Bot::startBot()
{
    std::cout << "Bot is started" << std::endl;
    while (isBotRunning()) {
        this->checkActions();
        this->process();
    }
    std::cout << "Bot is stopped" << std::endl;
}
