#include <assignment/utils.h>

static std::atomic_bool IS_SHUTTING_DOWN = false;

void assignment::abort()
{
    std::exit(EXIT_FAILURE);
}

void assignment::sigintHandel(int /*signal*/)
{
    IS_SHUTTING_DOWN = true;
}

bool assignment::isBotRunning()
{
    return !IS_SHUTTING_DOWN;
}

bool assignment::fillParameters(const nlohmann::json& jsonParameters, Parameters& parsedParameters)
{
    if (jsonParameters.find("bot") == jsonParameters.end()) {
        std::cout << "Key 'bot' must be present in config" << std::endl;
        return false;
    };
    if (!jsonParameters["bot"].is_object()) {
        std::cout << "Key 'bot' must be an object" << std::endl;
        return false;
    };
    if (!(jsonParameters["bot"].count("wheelRadius")
          && jsonParameters["bot"].count("wheelDistance")
          && jsonParameters["bot"].count("acceptanceDistance")
          && jsonParameters["bot"].count("acceptanceOrientation"))) {
        std::cout << "Keys 'bot.wheelRadius', 'bot.wheelDistance', 'bot.acceptanceDistance' and "
                     "'bot.acceptanceOrientation' must be present in config"
                  << std::endl;
        return false;
    };
    if (!(jsonParameters["bot"]["wheelRadius"].is_number_float()
          && jsonParameters["bot"]["wheelDistance"].is_number_float()
          && jsonParameters["bot"]["acceptanceDistance"].is_number_float()
          && jsonParameters["bot"]["acceptanceOrientation"].is_number_float())) {
        std::cout << "Keys 'bot.wheelRadius', 'bot.wheelDistance', 'bot.acceptanceDistance' and "
                     "'bot.acceptanceOrientation' must be the float numbers"
                  << std::endl;
        return false;
    };

    if (jsonParameters.find("server") == jsonParameters.end()) {
        std::cout << "Key 'server' must be present in config" << std::endl;
        return false;
    };
    if (!jsonParameters["server"].is_object()) {
        std::cout << "Key 'server' must be an object" << std::endl;
        return false;
    };
    if ((jsonParameters["server"].find("host") == jsonParameters["server"].end())
        || (jsonParameters["server"].find("port") == jsonParameters["server"].end())) {
        std::cout << "Keys 'server.host' and 'server.port' must be present in config" << std::endl;
        return false;
    };
    if (!jsonParameters["server"]["host"].is_string()
        || !jsonParameters["server"]["port"].is_number_unsigned()) {
        std::cout << "Keys 'server.host' must be a string and 'server.port' and an integer"
                  << std::endl;
        return false;
    };

    if ((jsonParameters["bot"]["wheelRadius"] <= 0.0)
        || (jsonParameters["bot"]["wheelRadius"] <= 0.0)) {
        std::cout << "Keys 'bot.wheelRadius' and 'bot.wheelDistance' must be the positive numbers"
                  << std::endl;
        return false;
    };

    if ((jsonParameters["bot"]["acceptanceDistance"] < 0.0)
        || (jsonParameters["bot"]["acceptanceOrientation"] < 0.0)) {
        std::cout << "Keys 'bot.acceptanceOrientation' and 'bot.acceptanceDistance' must not be "
                     "the negative numbers"
                  << std::endl;
        return false;
    };

    if (jsonParameters["server"]["host"] == "") {
        std::cout << "Key 'server.host' must not be an empty string" << std::endl;
        return false;
    };

    parsedParameters.botParameters.wheelRadius = jsonParameters["bot"]["wheelRadius"];
    parsedParameters.botParameters.wheelDistance = jsonParameters["bot"]["wheelDistance"];
    parsedParameters.botParameters.acceptanceDistance =
        jsonParameters["bot"]["acceptanceDistance"];
    parsedParameters.botParameters.acceptanceOrientation =
        jsonParameters["bot"]["acceptanceOrientation"];

    parsedParameters.serverAddress.host = jsonParameters["server"]["host"];
    parsedParameters.serverAddress.port = jsonParameters["server"]["port"];
    std::cout << "Parsed config successfully" << std::endl;
    return true;
}

bool assignment::processProgrammOptions(const int argc, std::vector<std::string>& argv,
                                        assignment::Parameters& parsedParameters)
{
    if (argc == 1) {
        std::cout << "Using: ./<bin> <path_to_config>" << std::endl;
        return false;
    }
    if (argc == 2) {
        const std::filesystem::path pathToConfig(argv[1]);
        if (!std::filesystem::exists(pathToConfig)) {
            std::cout << "File " << pathToConfig.c_str() << "doesn't exists." << std::endl;
            return false;
        }
        std::ifstream configStream(pathToConfig, std::ios::in);
        if (!configStream.is_open()) {
            std::cout << "Can't read from config file: " << pathToConfig.c_str() << std::endl;
            return false;
        }
        std::ostringstream configStr;
        configStr << configStream.rdbuf();

        nlohmann::json config = nlohmann::json::parse(configStr.str(), nullptr, false);
        if (config.is_discarded()) {
            std::cout << "Can't parse json" << std::endl;
            return false;
        }

        if (fillParameters(config, parsedParameters)) {
            return true;
        };
        return false;
    }
    return false;
}
