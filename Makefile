all:
	$(MAKE) tra-assignment --no-print-directory
clean:
	$(MAKE) tra-assignment-clean --no-print-directory

.PHONY: all test

CMAKE_FLAGS = -DCMAKE_EXPORT_COMPILE_COMMANDS=YES
CMAKE = cmake $(CMAKE_FLAGS)

BUILD_DIR = build

tra-assignment:
	mkdir --parents $(BUILD_DIR)
	rm -f compile_commands.json
	cd $(BUILD_DIR) && $(CMAKE) .. && $(MAKE) --no-print-directory
	ln -s $(BUILD_DIR)/compile_commands.json compile_commands.json

tra-assignment-clean:
	rm -rf $(BUILD_DIR)

PHONY_TARGETS += tra-assignment
CLEAN_TARGETS += tra-assignment-clean
