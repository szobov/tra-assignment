cmake_minimum_required(VERSION 3.14)
project(assignment)

list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

find_package(nlohmann_json 3.6.1 REQUIRED)
find_package(httplib REQUIRED)
find_package(Threads REQUIRED)

add_compile_options(-lstdc++fs -std=c++17 -Wall -Wextra -g3 -fno-omit-frame-pointer)

set(THREADS_PREFER_PTHREAD_FLAG ON)

include_directories(
  include
)

add_library(
  ${PROJECT_NAME}_utils
  src/utils.cpp
)

add_library(
  ${PROJECT_NAME}_control
  src/control.cpp
)

add_library(
  ${PROJECT_NAME}_server
  src/server.cpp
)

add_executable(
  ${PROJECT_NAME}
  src/main.cpp
)

target_link_libraries(${PROJECT_NAME}_utils
  stdc++fs
  nlohmann_json::nlohmann_json
)

target_link_libraries(${PROJECT_NAME}_server
  stdc++fs
  nlohmann_json::nlohmann_json
  Threads::Threads
)

target_link_libraries(${PROJECT_NAME}
  PRIVATE
  ${PROJECT_NAME}_utils
  ${PROJECT_NAME}_server
  ${PROJECT_NAME}_control
)
