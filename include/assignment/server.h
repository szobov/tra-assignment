#pragma once

#include <string>
#include <memory>
#include <thread>

#include <httplib/httplib.h>
#include <nlohmann/json.hpp>

#include <assignment/control.h>

namespace assignment {

class ServerAddress
{
public:
    std::string host;
    uint16_t port;
};

class Server : public std::enable_shared_from_this<Server>
{
private:
    httplib::Server server;
    assignment::Bot& bot;

    void postGoTo(const httplib::Request& request, httplib::Response& resp);
    void getStop(const httplib::Request& request, httplib::Response& resp);
    void getPause(const httplib::Request& request, httplib::Response& resp);
    void getResume(const httplib::Request& request, httplib::Response& resp);
    void getStatus(const httplib::Request& request, httplib::Response& resp);

    void listen(const ServerAddress& address);

    std::thread mainThread;

    static const constexpr char* jsonContentType = "application/json";
    enum class HTTP_STATUS { OK = 200, BAD_REQUEST = 400 };

    static void setResponse(httplib::Response& resp, const HTTP_STATUS status,
                            const nlohmann::json& body);

public:
    Server(assignment::Bot& bot);
    void init();
    void start(const ServerAddress& address);
    void stop();
};

} // namespace assignment
