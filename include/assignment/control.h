#pragma once

#include <atomic>
#include <queue>
#include <chrono>
#include <mutex>
#include <condition_variable>

#include <assignment/math.h>

namespace assignment {

class Target
{
public:
    Point2D position;
    double theta;
    double maxVelocity;
    double acceleration;

    Target(const double x, const double y, const double theta, const double maxVelocity,
           const double acceleration) :
        position(x, y),
        theta(normalizeAngle(theta)),
        maxVelocity(maxVelocity),
        acceleration(acceleration) {};
};

class WheelsVelocity
{
public:
    double left;
    double right;
    double tang;
    double linear;

    WheelsVelocity(const double left, const double right, const double tang, const double linear) :
        left(left),
        right(right),
        tang(tang),
        linear(linear)
    {}
};

class BotStatus
{
public:
    const Point2D position;
    const double theta;
    const WheelsVelocity velocity;

    BotStatus(const Point2D position, const double theta, const WheelsVelocity velocity) :
        position(position),
        theta(theta),
        velocity(velocity) {};
};

class BotParameters
{
public:
    double wheelRadius;
    double wheelDistance;
    double acceptanceDistance;
    double acceptanceOrientation;
};

using namespace std::chrono_literals;

class Bot
{
private:
    Point2D currentPosition;
    std::queue<Target> targets;
    std::unique_ptr<Target> currentTarget = nullptr;
    double theta;
    double targetTheta;
    WheelsVelocity velocity;
    WheelsVelocity prevVelocity;

    enum class State { IDLE, ROTATE_TO_POINT, MOVE_TO_POINT, ROTATE_TO_TARGET_THETA, FINISHED };
    enum class Actions { STOP, PAUSE, RESUME };
    std::atomic<Actions> currentAction = Actions::RESUME;

    State state = State::IDLE;

    const double wheelRadius;
    const double wheelDistance;

    const double acceptanceDistance;
    const double acceptanceOrientation;

    std::chrono::time_point<std::chrono::high_resolution_clock> lastTimestamp;
    std::chrono::time_point<std::chrono::high_resolution_clock> currentTimestamp;
    double dtUS;

    std::mutex wakeupLock;
    std::condition_variable wakeupCV;
    static const constexpr std::chrono::milliseconds wakeupTimeout = 500ms;

    static const uint8_t wheelsCount = 2;

    std::mutex statusLock;

    void process();
    void processState();
    void checkActions();
    bool processAction();
    void idle(bool popTarget);
    void setVelocityAndControl(const double left, const double right, const double tang,
                               const double linear);
    void control();
    void rotate(const double targetAngle);

public:
    Bot(const BotParameters& params);

    void setTarget(const Target& target);
    void stop();
    void pause();
    void resume();
    BotStatus status();

    void startBot();
};

} // namespace assignment
