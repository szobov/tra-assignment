#pragma once

#include <cmath>

namespace assignment {

class Point2D
{
public:
    Point2D(const double x, const double y) : x(x), y(y) {};
    double x, y;
};

inline double calcDistance(const Point2D& from, const Point2D& to)
{
    return std::sqrt(std::pow((to.x - from.x), 2) + std::pow((to.y - from.y), 2));
}

inline double normalizeAnglePositive(double angle)
{
    return std::fmod(std::fmod(angle, 2.0 * M_PI) + 2.0 * M_PI, 2.0 * M_PI);
}

inline double normalizeAngle(double angle)
{
    double a = normalizeAnglePositive(angle);
    if (a > M_PI) {
        a -= 2.0 * M_PI;
    }
    return a;
}

inline double shortestAngularDistance(double from, double to)
{
    return normalizeAngle(to - from);
}

inline double calcRotationToTarget(const Point2D& currentPosition, const Point2D& target,
                                   const double currentTheta)
{
    Point2D transformedVector(target.x - currentPosition.x, target.y - currentPosition.y);
    return shortestAngularDistance(currentTheta,
                                   std::atan2(transformedVector.y, transformedVector.x));
}

} // namespace assignment
