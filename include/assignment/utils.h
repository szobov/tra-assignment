#pragma once

#include <atomic>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <sstream>

#include <nlohmann/json.hpp>

#include <assignment/server.h>
#include <assignment/control.h>

namespace assignment {

void abort();

void sigintHandel(int);

bool isBotRunning();

class Parameters
{
public:
    assignment::BotParameters botParameters;
    assignment::ServerAddress serverAddress;
};

bool fillParameters(const nlohmann::json& jsonParameters, Parameters& parsedParameters);

bool processProgrammOptions(const int argc, std::vector<std::string>& argv,
                            Parameters& parsedParameters);

} // namespace assignment
