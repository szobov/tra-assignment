{ pkgs ? import <szobovpkgs>{} } :
let
  stdenv = pkgs.gcc8Stdenv;
in

let

pkgsList = with pkgs; [
  cmake
  cpp-httplib
  nlohmann_json
];

in

stdenv.mkDerivation rec {
  name = "tra-assignment";
  src = null;
  propagatedBuildInputs = [
  ] ++ pkgsList;
}
